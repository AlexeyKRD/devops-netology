# Домашние задания по курсу «DevOps и системное администрирование»

## Модуль 1. Введение в DevOps
+ 1.1 [Введение в DevOps](https://github.com/AlexeyKRD/devops-netology/tree/main/01-intro-01 "Домашнее задание к занятию «1.1. Введение в DevOps»")
## Модуль 2. Системы управления версиями
+ 2.1 [Системы контроля версий](https://github.com/AlexeyKRD/devops-netology/tree/main/02-git-01-vcs "Домашнее задание к занятию «2.1. Системы контроля версий.»") 
+ 2.2 [Основы Git](https://github.com/AlexeyKRD/devops-netology/tree/main/02-git-02-base)
+ 2.3 [Ветвления в Git](https://github.com/AlexeyKRD/devops-netology/tree/main/02-git-03-branching) 
+ 2.4 [Инструменты Git](https://github.com/AlexeyKRD/devops-netology/tree/main/02-git-04-tools) 